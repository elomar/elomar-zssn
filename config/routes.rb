Rails.application.routes.draw do
  resources :survivors, only: [:index, :show, :create] do 
    resource :location, only: [:show, :update]
    resources :contamination_reports, only: [:create]
  end

  resources :trades, only: [:create]

  scope "/reports" do
    get "/contaminated", to: "reports#contaminated"
    get "/non-contaminated", to: "reports#non_contaminated"
    get "/resources-per-survivor", to: "reports#resources_per_survivor"
    get "/points-lost", to: "reports#points_lost"
  end
end
