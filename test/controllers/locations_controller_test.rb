require 'test_helper'

class LocationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @survivor = survivors(:one).dup
    @survivor.save
  end

  test "should show location" do
    get survivor_location_url(@survivor), as: :json
    assert_response :success
    assert_equal @survivor.last_location.to_json, @response.body
  end

  test "should update location" do
    patch survivor_location_url(@survivor), params: { location: [-5.820,-35.212] }, as: :json
    assert_response 201
  end
end
