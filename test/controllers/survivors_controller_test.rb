require 'test_helper'

class SurvivorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @survivor = survivors(:one)
  end

  test "should list survivors" do
    get survivors_url

    assert_response 200
  end

  test "should show survivor" do
    get survivor_url(@survivor)

    assert_response 200
  end

  test "should create survivor" do
    assert_difference(['Survivor.count', 'InventoryItem.count']) do
      post survivors_url, 
        params: { survivor: { age: @survivor.age, gender: @survivor.gender, last_location: @survivor.last_location, name: @survivor.name, inventory: { Resource.first.name => 5 } } }, 
        as: :json
    end

    assert_response 201
  end
end
