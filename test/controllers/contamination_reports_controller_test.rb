require 'test_helper'

class ContaminationReportsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @reporter = survivors(:one)
    @contaminated = survivors(:two)
  end

  test "should create report" do
    assert_difference('ContaminationReport.count') do
      post survivor_contamination_reports_url(@contaminated.id), 
        params: { reporter_id: @reporter.id },
        as: :json
    end

    assert_response 201
  end
end
