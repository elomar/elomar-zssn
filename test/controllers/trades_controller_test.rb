require 'test_helper'

class TradesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @resource_1_point = Resource.create!(name: "1 point", points: 1)
    @resource_2_points = Resource.create!(name: "2 points", points: 2)

    @survivor_1 = survivors(:one)
    @survivor_1.inventory_items.create!(resource: @resource_1_point, quantity: 2)

    @survivor_2 = survivors(:two)
    @survivor_2.inventory_items.create!(resource: @resource_2_points, quantity: 1)
  end

  test "should create valid trade" do
    post trades_url,
      params: { parties: [
        { party_id: @survivor_1.id, items: { @resource_1_point.name => 2 } },
        { party_id: @survivor_2.id, items: { @resource_2_points.name => 1 } }
      ]},
      as: :json

    assert_response :created

    assert @survivor_1.owns?(@resource_2_points, 1)
    assert @survivor_2.owns?(@resource_1_point, 2)
  end

  test "should not create invalid trade" do
    post trades_url,
      params: { parties: [
        { party_id: @survivor_1.id, items: { @resource_1_point.name => 1 } },
        { party_id: @survivor_2.id, items: { @resource_2_points.name => 1 } }
      ]},
      as: :json

    assert_response :unprocessable_entity

    assert @survivor_1.owns?(@resource_1_point, 2)
    assert @survivor_2.owns?(@resource_2_points, 1)
  end
end
