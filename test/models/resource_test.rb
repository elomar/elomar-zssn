require 'test_helper'

class ResourceTest < ActiveSupport::TestCase
  test "should find by name" do
    assert_equal Resource.find_by(name: "Water"), Resource.named("Water")
  end
end
