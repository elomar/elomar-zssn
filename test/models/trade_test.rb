require 'test_helper'

class TradeTest < ActiveSupport::TestCase
  setup do 
    @resource_1_point = Resource.create!(name: "1 point", points: 1)
    @resource_2_points = Resource.create!(name: "2 points", points: 2)

    @survivor_1 = survivors(:one)
    @survivor_1.inventory_items.create!(resource: @resource_1_point, quantity: 2)

    @survivor_2 = survivors(:two)
    @survivor_2.inventory_items.create!(resource: @resource_2_points, quantity: 1)
  end

  test "should not be valid when points are unbalanced" do
    trade = Trade.new([
      { party: @survivor_1, items: { @resource_1_point => 1 } },
      { party: @survivor_2, items: { @resource_2_points => 1 } }
    ])

    assert_not trade.valid?
  end

  test "should be valid when points are balanced" do
    trade = Trade.new([
      { party: @survivor_1, items: { @resource_1_point => 2 } },
      { party: @survivor_2, items: { @resource_2_points => 1 } }
    ])

    assert trade.valid?
  end

  test "should not be valid when parties don't own enough items" do
    trade = Trade.new([
      { party: @survivor_1, items: { @resource_1_point => 4 } },
      { party: @survivor_2, items: { @resource_2_points => 2 } }
    ])

    assert_not trade.valid?
  end
  
  test "should not be valid when party is contaminated" do
    @survivor_1.update_attribute :contaminated, true

    trade = Trade.new([
      { party: @survivor_1, items: { @resource_1_point => 2 } },
      { party: @survivor_2, items: { @resource_2_points => 1 } }
    ])

    assert_not trade.valid?

    @survivor_1.update_attribute :contaminated, false
  end
  
  test "should transfer items when performed" do
    trade = Trade.new([
      { party: @survivor_1, items: { @resource_1_point => 2 } },
      { party: @survivor_2, items: { @resource_2_points => 1 } }
    ])

    trade.perform

    assert @survivor_1.owns?(@resource_2_points, 1)
    assert @survivor_2.owns?(@resource_1_point, 2)
  end

  test "should not be performed when invalid" do
    trade = Trade.new([
      { party: @survivor_1, items: { @resource_1_point => 4 } },
      { party: @survivor_2, items: { @resource_2_points => 2 } }
    ])

    assert_not trade.perform

    assert @survivor_1.owns?(@resource_1_point, 2)
    assert @survivor_2.owns?(@resource_2_points, 1)
  end
end
