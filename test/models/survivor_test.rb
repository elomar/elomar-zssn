require 'test_helper'

class SurvivorTest < ActiveSupport::TestCase
  setup do
    @survivor = Survivor.new(
      name: "Survivor",
      gender: "female",
      age: 50,
      last_location: [-23.6186,-46.6732]
    )
  end

  test "should only allow male, female as genders" do
    ["male", "female"].each do |valid_gender|
      @survivor.gender = valid_gender
      assert @survivor.valid?
    end

    ["invalid", 0, nil].each do |invalid_gender|
      @survivor.gender = invalid_gender
      assert_not @survivor.valid?
    end
  end

  test "should only allow positive ages" do
    @survivor.age = -5
    assert_not @survivor.valid?
  end

  test "should return inventory" do
    @survivor.save!
    @survivor.inventory_items.create!(resource: Resource.named("Water"), quantity: 2)

    assert_equal({ "Water" => 2 }, @survivor.inventory)
  end

  test "should create inventory" do
    survivor = Survivor.new(
      inventory: { Resource.first.name => 2 }
    )

    assert_equal 1, survivor.inventory_items.size
    assert_equal Resource.first, survivor.inventory_items.first.resource
    assert_equal 2, survivor.inventory_items.first.quantity
  end

  test "should flag as contaminated" do
    @survivor.save!

    assert_not @survivor.contaminated

    Survivor::CONTAMINATION_THRESHOLD.times do ||
      reporter = @survivor.dup
      @survivor.contamination_reports.create!(reporter: reporter)
    end

    assert @survivor.contaminated
  end

  test "should know if survivor owns enough of a given resource" do
    @survivor.save!
    @survivor.inventory_items.create!(resource: Resource.named("Water"), quantity: 2)
    
    assert @survivor.owns?(Resource.named("Water"), 2)
    assert_not @survivor.owns?(Resource.named("Water"), 3)
    assert_not @survivor.owns?(Resource.named("Ammunition"), 1)
  end

  test "should transfer from one survivor to the other" do
    @survivor.save!
    @survivor.inventory_items.create!(resource: Resource.named("Water"), quantity: 2)
    
    survivor_2 = @survivor.dup
    survivor_2.save!

    @survivor.transfer_to(survivor_2, Resource.named("Water") => 1)

    assert @survivor.owns?(Resource.named("Water"), 1)
    assert_not @survivor.owns?(Resource.named("Water"), 2)
    assert survivor_2.owns?(Resource.named("Water"), 1)
  end

  test "should raise an error when invalid transfer is attempted" do
    @survivor.save!
    @survivor.inventory_items.create!(resource: Resource.named("Water"), quantity: 2)
    
    survivor_2 = @survivor.dup
    survivor_2.save!

    assert_raise RuntimeError do
      @survivor.transfer_to(survivor_2, Resource.named("Water") => 4)
    end

    assert @survivor.owns?(Resource.named("Water"), 2)
    assert_not survivor_2.owns?(Resource.named("Water"), 1)
  end
end
