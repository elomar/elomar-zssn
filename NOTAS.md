# Notas

Como esse projeto foi proposto como um desafio com um tempo relativamente limitado pra ser feito, eu queria explicar algumas decisões que acabei tomando pra quem for revisar isso aqui:

* Algumas coisas (por exemplo, Trade ser um objeto no lugar de um método no controller, ou Report.resources\_per\_survivor usar uns lances SQL meio complicados) são definitivamente otimização prematura. Fiz desse jeito meio que imaginando esse desafio como parte de um projeto maior onde esse tipo de coisa faria mais diferença;
* Em relação aos testes, usei fixtures mas sinto que trocar por Factories já seria o próximo passo;
* Os relatórios não foram testados, achei que iria precisar de Factories pra gerar dado suficiente e que não ia adicionar muita coisa ao desafio, fosse um projeto de verdade ia ter teste;
* Documentação tá em inglês só porque a descrição já tava em inglês também.

Qualquer coisa, só chamar.