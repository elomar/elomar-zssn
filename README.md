# ZSSN (Zombie Survival Social Network)

## Problem Description

I was given a challenge:

> The world as we know it has fallen into an apocalyptic scenario. A laboratory-made virus is transforming human beings and animals into zombies, hungry for fresh flesh.

> You, as a zombie resistance member (and the last survivor who knows how to code), was designated to develop a system to share resources between non-infected humans.

This is my answer to it. Go resistance.

## API Documentation

_note: the sample calls provided use [httpie](http://httpie.org/)_

### List Survivors

----

* **URL**

  /survivors

* **Method:**
  
  `GET`
  
* **Success Response:**

  * **Code:** 200 <br />
    **Content:** _list of survivors data, including inventory_
 
* **Sample Call:**

  `http elomar-zssn.herokuapp.com/survivors`

### Show Survivor

----

* **URL**

  /survivors/:id

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   `id=[integer]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ survivor: [` _survivor data, including inventory_ `] }`

* **Sample Call:**

  `http elomar-zssn.herokuapp.com/survivors/1`

### Create Survivor

----

* **URL**

  /survivors

* **Method:**

  `POST`
  
* **Data Params**

  `survivor: name=[string], age=[integer], gender=[male | female], last\_location=[lat, long], inventory=[`_dictionary with resources as keys, quantities as values_`]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ survivor: [`_survivor data, including inventory_`] }`

* **Sample Call:**

  `http POST elomar-zssn.herokuapp.com/survivors survivor:='{"name": "Survivor", "gender": "male", "age": "25", "last\_location": [-23.6186,-46.6732], "inventory": { "Water": 4 }}'`

### Fetch Survivor Location

----

Fetch last known location of a survivor

* **URL**

  /survivors/:id/location

* **Method:**

  `GET`
  
* **URL Params**

  `id=[integer]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `[lat, long]`

* **Sample Call:**

  `http elomar-zssn.herokuapp.com/survivors/1/location`

### Update Survivor Location

----

Update last known location of a survivor

* **URL**

  /survivors/:id/location

* **Method:**

  `PATCH` or `PUT`
  
* **URL Params**

  `id=[integer]`

* **Data Params**

  `location: location=[lat, long]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `[lat, long]`

* **Sample Call:**

  `http PATCH elomar-zssn.herokuapp.com/survivors/1/location location:='[-5.820,-35.212]'`

### Submit contamination report

----

Submits a report that a given survivor has been contaminated. A survivor that is reported contaminated 3 times is flagged as contaminated and cannot trade with other survivors.

* **URL**

  /survivors/:id/contamination_reports

* **Method:**

  `POST`
  
* **URL Params**

  `id=[integer]`

* **Data Params**

  `contamination\_report: reporter\_id=[integer]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ contamination\_report: {` _report data_ `} }`

* **Sample Call:**

  `http POST elomar-zssn.herokuapp.com/survivors/20/contamination\_reports contamination\_report:='{"reporter\_id": 21}'

### Trade

----

Performs a trade with another survivor. In order for the trade to be successful both sides must be trading equal amounts of points; actually own the items they intend to trade; and not have been flagged as contaminated.

* **URL**

  /trades

* **Method:**

  `POST`

* **Data Params**

  `parties: [{party_id:` _survivor id_`, items:` _dictionary with resources as keys, quantities as values_`}]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ parties:` _trade data_ `}`

* **Sample Call:**

  `http elomar-zssn.herokuapp.com/trades parties:='[{"party_id": 21, "items": {"Medication": 1}}, {"party_id": 20, "items": {"Ammunition": 2}}]'`

### Reports

----

Fetch report data on a few topics related to the state of the world. Each report has its own response format, please refer to `Report` class for their formats and meaning.

* **URL**

  /reports/:report

* **Method:**

  `GET`

* **URL Params**

  `report=[contaminated | non-contaminated | resources-per-survivor | points-lost]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ data:` _report data, varies by report_ `}`

* **Sample Call:**

  `http elomar-zssn.herokuapp.com/reports/infected`
