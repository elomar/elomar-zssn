class ContaminationReport < ApplicationRecord
  belongs_to :reporter, class_name: 'Survivor'
  belongs_to :contaminated, class_name: 'Survivor'

  validates :reporter, uniqueness: { scope: :contaminated }
end
