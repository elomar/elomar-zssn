class Trade
  def initialize(parties)
    @parties = parties
  end

  def valid?
    balanced? && parties_own_items? && parties_not_contaminated?
  end

  def perform
    if valid?
      ActiveRecord::Base.transaction do
        @parties[0][:party].transfer_to(@parties[1][:party], @parties[0][:items])
        @parties[1][:party].transfer_to(@parties[0][:party], @parties[1][:items])
      end
    end
  end

  private

  def balanced?
    balance(@parties[0][:items]) == balance(@parties[1][:items])
  end

  def balance(items)
    items.sum {|resource, quantity| resource.points * quantity }
  end

  def parties_own_items?
    party_owns_items?(@parties[0]) && party_owns_items?(@parties[1])
  end

  def party_owns_items?(party)
    party[:items].all? {|resource, quantity| party[:party].owns?(resource, quantity)}
  end

  def parties_not_contaminated?
    not (@parties[0][:party].contaminated? or @parties[1][:party].contaminated?)
  end
end
