class Survivor < ApplicationRecord
  CONTAMINATION_THRESHOLD = 3

  validates :name, :age, :gender, :last_location, presence: true
  validates :gender, inclusion: { in: %w{male female} }
  validates :age, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  has_many :inventory_items, inverse_of: :survivor, dependent: :destroy
  validates_associated :inventory_items

  has_many :contamination_reports, foreign_key: 'contaminated_id', after_add: :flag_as_contaminated

  scope :contaminated, -> { where(contaminated: true) }

  attribute :last_location, :legacy_point

  def owns?(resource, quantity)
    quantity_owned = inventory_items.find_by(resource: resource).try(:quantity) || 0
    quantity_owned >= quantity
  end

  def transfer_to(target_survivor, items)
    raise "not enough inventory to transfer" unless items.all? {|resource, quantity| owns?(resource, quantity)}

    items.each do |resource, quantity|
      inventory_items.find_by(resource: resource).decrement!(:quantity, quantity)
      target_survivor.inventory_items.find_or_create_by(resource: resource).increment!(:quantity, quantity)
    end
  end

  def inventory
    inventory_items.includes(:resource).inject({}) {|inventory, item|
      inventory[item.resource.name] = item.quantity
      inventory
    }
  end

  def inventory=(items)
    items.each do |resource, quantity|
      inventory_items.build(resource: Resource.named(resource), quantity: quantity)
    end
  end

  def as_json(args = {})
    super({ methods: :inventory }.merge(args))
  end

  private

  def flag_as_contaminated(contamination_report)
    if contamination_reports.count == CONTAMINATION_THRESHOLD
      update_attribute :contaminated, true
    end
  end
end
