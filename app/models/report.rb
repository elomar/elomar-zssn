class Report
  def self.contaminated
    if Survivor.count > 0
      Survivor.contaminated.count.to_f / Survivor.count
    else
      0
    end
  end

  def self.non_contaminated
    1 - contaminated
  end

  def self.resources_per_survivor
    Resource.
      joins("join inventory_items on inventory_items.resource_id = resources.id").
      select("resources.id, resources.name, sum(inventory_items.quantity::float)/(select count(id) from survivors where contaminated = false) as average").
      group("resources.id, resources.name").
      as_json(only: [:name, :average])
  end

  def self.points_lost
    InventoryItem.includes(:survivor, :resource).where(survivors: { contaminated: true }).sum("inventory_items.quantity * resources.points")
  end
end
