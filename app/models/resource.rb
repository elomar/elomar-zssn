class Resource < ApplicationRecord
  validates :name, :points, presence: true
  validates :name, uniqueness: true

  def self.named(name)
    find_by name: name
  end

  def to_s
    name
  end
end
