class SurvivorsController < ApplicationController
  def index
    @survivors = Survivor.includes(:inventory_items).all

    render json: @survivors
  end

  def show
    @survivor = Survivor.find(params[:id])

    render json: @survivor
  end

  def create
    @survivor = Survivor.new(survivor_params)

    if @survivor.save
      render json: @survivor, status: :created, location: survivor_location_url(@survivor)
    else
      render json: @survivor.errors, status: :unprocessable_entity
    end
  end

  private
    def survivor_params
      params.require(:survivor).permit(:name, :age, :gender, last_location: [], inventory: Resource.pluck(:name))
    end
end
