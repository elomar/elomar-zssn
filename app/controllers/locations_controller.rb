class LocationsController < ApplicationController
  def show
    @survivor = Survivor.find(params[:survivor_id])
    render json: @survivor.last_location
  end
  
  def update
    @survivor = Survivor.find(params[:survivor_id])

    if @survivor.update_attribute :last_location, params[:location]
      render json: @survivor.last_location, status: :created, location: survivor_location_url(@survivor)
    else
      render json: @survivor.errors, status: :unprocessable_entity
    end
  end
end
