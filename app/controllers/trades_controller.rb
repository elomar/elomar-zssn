class TradesController < ApplicationController
  def create
    @trade = Trade.new(trade_params)

    if @trade.perform
      render json: @trade, status: :created, location: survivor_location_url(first_survivor_id)
    else
      render json: "", status: :unprocessable_entity
    end
  end

  private

  def trade_params
    params.to_unsafe_hash[:trade][:parties].map {|party|
      { 
        party: Survivor.find(party[:party_id]),
        items: party[:items].inject({}) {|items, item|
          items[Resource.named(item[0])] = item[1]
          items
        }
      }
    }
  end

  def first_survivor_id
    params[:trade][:parties][0][:party_id]
  end
end

