class ReportsController < ApplicationController
  def contaminated
    render json: { data: Report.contaminated }
  end

  def non_contaminated
    render json: { data: Report.non_contaminated }
  end

  def resources_per_survivor
    render json: { data: Report.resources_per_survivor }
  end

  def points_lost
    render json: { data: Report.points_lost }
  end
end
