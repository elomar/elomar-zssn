class ContaminationReportsController < ApplicationController
  def create
    @survivor = Survivor.find(params[:survivor_id])
    @contamination_report = @survivor.contamination_reports.new(contamination_report_params)

    if @contamination_report.save
      render json: @contamination_report, status: :created, location: survivor_location_url(@survivor)
    else
      render json: @contamination_report.errors, status: :unprocessable_entity
    end
  end

  private
    def contamination_report_params
      params.require(:contamination_report).permit(:reporter_id)
    end
end
