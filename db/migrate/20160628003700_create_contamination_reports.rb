class CreateContaminationReports < ActiveRecord::Migration[5.0]
  def change
    create_table :contamination_reports do |t|
      t.integer :reporter_id, foreign_key: true
      t.integer :contaminated_id, foreign_key: true

      t.timestamps
    end
  end
end
