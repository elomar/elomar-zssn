class AddContaminatedToSurvivors < ActiveRecord::Migration[5.0]
  def change
    add_column :survivors, :contaminated, :boolean, default: false
  end
end
