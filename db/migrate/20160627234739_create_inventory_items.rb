class CreateInventoryItems < ActiveRecord::Migration[5.0]
  def change
    create_table :inventory_items do |t|
      t.references :resource, foreign_key: true
      t.references :survivor, foreign_key: true
      t.integer :quantity, default: 0, null: false

      t.timestamps
    end
  end
end
